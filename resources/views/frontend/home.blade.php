@extends('layouts.frontend')

@section('head_and_title')
    <meta name="description" content="BNW Home">
    <meta name="author" content="PT. Generasi Muda Gigih">
    <meta name="keywords"
          content="Ben and Wyatt, Branding House, Design House, Office, Residential, Apartment, Commercial, Spaces">

    <title>BNW - HOME</title>
@endsection

@section('content')
{{--    <section class="header-img">--}}
{{--        <div class="container pt-5 pt-sm-0 pb-5">--}}
{{--            <div>--}}
{{--                <div class="row txt-banner-spacing pb-5">--}}
{{--                    <div--}}
{{--                        class="col-sm-8 col-md-8 pt-sm-3 pt-md-0 text-white h-100 d-flex justify-content-center flex-column">--}}
{{--                        <p class="pb-3 pt-5 font-av-next let-spa-3 txt-subheader">FROM US TO YOU</p>--}}
{{--                        <p class="font-didot-bold mb-md-3 mb-2 txt-header-main">--}}
{{--                            Dear Founders,<br/>--}}
{{--                            Be Proud.--}}
{{--                        </p>--}}
{{--                        <p class="custom-font-avenir-medium  d-block d-md-none"--}}
{{--                           style="font-size:13px;">--}}
{{--                            A bit of small talk, something really general<br/>to bridge--}}
{{--                            an introduction to Ben & Wyatt.<br/> Needs just around--}}
{{--                            this much of texts.--}}
{{--                        </p>--}}
{{--                        <p class="font-av-next custom-line-height d-none d-md-block txt-body2 mt-md-3 mt-2">--}}
{{--                            Design not only gives you chance in the marketplace,<br/>--}}
{{--                            but it makes your brand looks good, makes you look good,<br/>--}}
{{--                            make you proud to show those hardwork.</p>--}}
{{--                    </div>--}}
{{--                    <div class="col-sm-4 col-md-6"></div>--}}
{{--                </div>--}}
{{--                --}}{{--Desktop--}}
{{--                <div class="row d-none d-md-inline pt-5 pl-md-4">--}}
{{--                    <div class="col swiper-container swiper-container-desktop-header">--}}
{{--                        <div class="swiper-wrapper">--}}
{{--                            <div class="swiper-slide">--}}
{{--                                <img src="{{ asset('images/bnw/home/bnw-web_3.0-37.png') }}" class="img-banner">--}}
{{--                            </div>--}}
{{--                            <div class="swiper-slide">--}}
{{--                                <img src="{{ asset('images/bnw/home/bnw-web_3.0-38.png') }}" class="img-banner">--}}
{{--                            </div>--}}
{{--                            <div class="swiper-slide">--}}
{{--                                <img src="{{ asset('images/bnw/home/bnw-web_3.0-39.png') }}" class="img-banner">--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    --}}{{--<div class="col-1 center-vertical-horizontal">--}}
{{--                        <i class="fas fa-chevron-right"></i>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="row d-none d-md-inline">--}}
{{--                    <div class="border-custom-green"></div>--}}
{{--                </div>--}}
{{--            </div>--}}

{{--            --}}{{--Mobile--}}
{{--            <div class="d-block d-md-none">--}}
{{--                <div class="row">--}}
{{--                    <div class="col-11">--}}
{{--                        <div class="swiper-container swiper-container-mobile-header">--}}
{{--                            <div class="swiper-wrapper">--}}
{{--                                <div class="swiper-slide">--}}
{{--                                    <img src="{{ asset('images/bnw/home/bnw-web_3.0-37.png') }}" class="img-banner">--}}
{{--                                </div>--}}
{{--                                <div class="swiper-slide">--}}
{{--                                    <img src="{{ asset('images/bnw/home/bnw-web_3.0-38.png') }}" class="img-banner">--}}
{{--                                </div>--}}
{{--                                <div class="swiper-slide">--}}
{{--                                    <img src="{{ asset('images/bnw/home/bnw-web_3.0-39.png') }}" class="img-banner">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                    <div class="col-1 center-vertical-horizontal">--}}
{{--                        <i class="fas fa-chevron-right fa-chevron-right-white swiper-container-mobile-header-next"></i>--}}
{{--                    </div>--}}
{{--                    --}}{{--<div class="col-auto d-inline">--}}
{{--                        <i class="fas fa-chevron-right"></i>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="row">--}}
{{--                    <div class="border-custom-green"></div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}

    <section style="margin-top: -100px;">
        <div class=container">
            <div class="row no-gutters">
                <div class="col-12">
                    <div id="bnw_banner">
                        <div class="bnw_banner_background bnw_banner_1">
                            <div class="row pb-5 pt-percent-banner no-gutters">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-7 col-md-6 text-white h-100 d-flex justify-content-center flex-column">
                                    <div class="w-100 pl-lg-0 pl-md-0 ml-em-banner">
                                        <p class="pb-3 pt-5 font-av-next let-spa-3 txt-subheader">FROM US TO YOU</p>
                                        <p class="font-didot-bold mb-md-3 mb-2 txt-header-main">
                                            Dear Founders,<br/>
                                            Be Proud.
                                        </p>
                                        <p class="d-none d-md-block font-av-next custom-line-height txt-body2 mt-md-3 mt-2">
                                            More than setting a spotlight,<br/>
                                            a curated design makes your brand look good, makes you look good,<br/>
                                            and makes you proud of the hard work poured<br/></p>
                                        <p class="d-block d-md-none font-av-next custom-line-height txt-body2 mt-md-3 mt-2 mr-5">
                                            More than setting a spotlight,
                                            a curated design makes your brand look good, makes you look good,
                                            and makes you proud of the hard work poured</p>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4"></div>
                            </div>
                        </div>
                        <div class="bnw_banner_background bnw_banner_2">
                            <div class="row pb-5 pt-percent-banner no-gutters">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-7 col-md-6 text-white h-100 d-flex justify-content-center flex-column">
                                    <div class="w-100 pl-lg-0 pl-md-0 ml-em-banner">
                                        <p class="pb-3 pt-5 font-av-next let-spa-3 txt-subheader">WE ARE ON YOUR SIDE</p>
                                        <p class="font-didot-bold mb-md-3 mb-2 txt-header-main">
                                            As Your Designer<br/>
                                            & Copywriter
                                        </p>
                                        <p class="font-av-next custom-line-height txt-body2 mt-md-3 mt-2">
                                            We are your squad in providing quality custom branding,<br/>
                                            marketing tools designs, and professional copywriting.<br/>
                                            We are on your side!
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4"></div>
                            </div>
                        </div>
                        <div class="bnw_banner_background bnw_banner_3">
                            <div class="row pb-5 pt-percent-banner no-gutters">
                                <div class="col-lg-1"></div>
                                <div class="col-lg-7 col-md-6 text-white h-100 d-flex justify-content-center flex-column">
                                    <div class="w-100 pl-lg-0 pl-md-0 ml-em-banner">
                                        <p class="pb-3 pt-5 font-av-next let-spa-3 txt-subheader">OUR DAILY TASK</p>
                                        <p class="font-didot-bold mb-md-3 mb-2 txt-header-main">
                                            Mission<br/>
                                            Possible
                                        </p>
                                        <p class="font-av-next custom-line-height txt-body2 mt-md-3 mt-2">
                                            Our daily task is applying creativity, experience, integrity and<br/>
                                            dedication to deliver exceptional outcomes for<br/>
                                            our respected clients.
                                        </p>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- Video --}}
    <section style="background-color: black">
        <video class="w-100 pt-5" autoplay muted loop>
            <source src="{{ asset('videos/bnw/home/web-intro.mp4') }}" type="video/mp4">
        </video>
        {{--Your browser does not support the video tag.--}}
        {{--<iframe class="w-100" src="{{ asset('videos/bnw/home/web-intro.mp4') }}" allow="autoplay; encrypted-media"></iframe>--}}
    </section>

    {{--Services Desktop--}}
    <section id="services" class="text-white py-5 d-none d-md-block" style="background-color: black">
        {{--Our Services Desktop--}}
        <div class="container">
            <div class="row pb-5">
                <div class="col-12">
                    <p class="font-av-next txt-subheader">OUR SERVICES</p>
                    <p class="font-didot-bold txt-what">What We Do</p>
                </div>
            </div>
            <div class="row pb-5 mb-5">
                <div class="col-md-3">
                    <img src="{{ asset('images/bnw/home/bnw-web-icons_3.2-40.png') }}" class="logo-services pb-2">
                    <p class="txt-services-title">Project Concept</p>
                    <p class="txt-services-body"> Design not only give you chance
                        in the marketplace, but it makes your
                        brand looks good, makes you.
                    </p>
                </div>
                <div class="col-md-3">
                    <img src="{{ asset('images/bnw/home/bnw-web-icons_3.2-41.png') }}" class="logo-services pb-2">
                    <p class="txt-services-title">Logo & Naming</p>
                    <p class="txt-services-body"> Design not only give you chance
                        in the marketplace, but it makes your
                        brand looks good, makes you.
                    </p>
                </div>
                <div class="col-md-3">
                    <img src="{{ asset('images/bnw/home/bnw-web-icons_3.2-42.png') }}" class="logo-services pb-2">
                    <p class="txt-services-title">Promotional Copywriting</p>
                    <p class="txt-services-body"> Design not only give you chance
                        in the marketplace, but it makes your
                        brand looks good, makes you.
                    </p>
                </div>
                <div class="col-md-3">
                    <img src="{{ asset('images/bnw/home/bnw-web-icons_3.2-43.png') }}" class="logo-services pb-2">
                    <p class="txt-services-title">Illustrative Design</p>
                    <p class="txt-services-body"> Design not only give you chance
                        in the marketplace, but it makes your
                        brand looks good, makes you.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <img src="{{ asset('images/bnw/home/bnw-web-icons_3.2-44.png') }}" class="logo-services pb-2">
                    <p class="txt-services-title">Company Profile Design</p>
                    <p class="txt-services-body"> Design not only give you chance
                        in the marketplace, but it makes your
                        brand looks good, makes you.
                    </p>
                </div>
                <div class="col-md-3">
                    <img src="{{ asset('images/bnw/home/bnw-web-icons_3.2-45.png') }}" class="logo-services pb-2">
                    <p class="txt-services-title">Website Development</p>
                    <p class="txt-services-body"> Design not only give you chance
                        in the marketplace, but it makes your
                        brand looks good, makes you.
                    </p>
                </div>
                <div class="col-md-3">
                    <img src="{{ asset('images/bnw/home/bnw-web-icons_3.2-46.png') }}" class="logo-services pb-2">
                    <p class="txt-services-title">Marketing Tools Design</p>
                    <p class="txt-services-body"> Design not only give you chance
                        in the marketplace, but it makes your
                        brand looks good, makes you.
                    </p>
                </div>
                <div class="col-md-3">
                    <img src="{{ asset('images/bnw/home/bnw-web-icons_3.2-47.png') }}" class="logo-services pb-2">
                    <p class="txt-services-title">Stationaries Design</p>
                    <p class="txt-services-body"> Design not only give you chance
                        in the marketplace, but it makes your
                        brand looks good, makes you.
                    </p>
                </div>
            </div>
        </div>
    </section>

    {{--Portfolio--}}
    {{--<section id="works" class="py-5 py-sm-5 pt-sm-5" style="background-color: black;">
        --}}{{--Portfolio Desktop--}}{{--
        <div class="container">
            <div class="text-white">
                --}}{{--Title--}}{{--
                <div>
                    <p class="font-av-next txt-subheader">OUR WORKS</p>
                    <p class="font-didot txt-what">Curated Portfolios</p>
                </div>
            </div>
        </div>
        --}}{{--Porto Swiper--}}{{--
        <div class="mt-md-3 swiper-container swiper-container-portfolio">
            <!-- Add Scrollbar -->
            <div class="swiper-scrollbar swiper-scrollbar-portfolio pr-2"></div>
            <div class="swiper-wrapper swiper-wrapper-portfolio pl-md-5">
                <div class="swiper-slide">
                    <a href="{{route('frontend.portfolio.show', ['type'=> 1])}}">
                        <img class="portfolio-img" src="{{ asset('images/bnw/portfolio/bnw-web_3.0-13.jpg') }}"/>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="{{route('frontend.portfolio.show', ['type'=> 2])}}">
                        <img class="portfolio-img" src="{{ asset('images/bnw/portfolio/bnw-web_3.0-14.jpg') }}"/>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="{{route('frontend.portfolio.show', ['type'=> 3])}}">
                        <img class="portfolio-img" src="{{ asset('images/bnw/portfolio/bnw-web_3.0-15.jpg') }}"/>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="{{route('frontend.portfolio.show', ['type'=> 4])}}">
                        <img class="portfolio-img" src="{{ asset('images/bnw/portfolio/bnw-web_3.0-16.jpg') }}"/>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="{{route('frontend.portfolio.show', ['type'=> 5])}}">
                        <img class="portfolio-img" src="{{ asset('images/bnw/portfolio/bnw-web_3.0-17.jpg') }}"/>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="{{route('frontend.portfolio.show', ['type'=> 6])}}">
                        <img class="portfolio-img" src="{{ asset('images/bnw/portfolio/bnw-web_3.0-18.jpg') }}"/>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="{{route('frontend.portfolio.show', ['type'=> 7])}}">
                        <img class="portfolio-img" src="{{ asset('images/bnw/portfolio/bnw-web_3.0-19.jpg') }}"/>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="{{route('frontend.portfolio.show', ['type'=> 8])}}">
                        <img class="portfolio-img" src="{{ asset('images/bnw/portfolio/bnw-web_3.0-20.jpg') }}"/>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="{{route('frontend.portfolio.show', ['type'=> 9])}}">
                        <img class="portfolio-img" src="{{ asset('images/bnw/portfolio/bnw-web_3.0-27.jpg') }}"/>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="{{route('frontend.portfolio.show', ['type'=> 10])}}">
                        <img class="portfolio-img" src="{{ asset('images/bnw/portfolio/bnw-web_3.0-28.jpg') }}"/>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="{{route('frontend.portfolio.show', ['type'=> 11])}}">
                        <img class="portfolio-img" src="{{ asset('images/bnw/portfolio/bnw-web_3.0-29.jpg') }}"/>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="{{route('frontend.portfolio.show', ['type'=> 12])}}">
                        <img class="portfolio-img" src="{{ asset('images/bnw/portfolio/bnw-web_3.0-30.jpg') }}"/>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="{{route('frontend.portfolio.show', ['type'=> 13])}}">
                        <img class="portfolio-img" src="{{ asset('images/bnw/portfolio/bnw-web_3.0-31.jpg') }}"/>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="{{route('frontend.portfolio.show', ['type'=> 14])}}">
                        <img class="portfolio-img" src="{{ asset('images/bnw/portfolio/bnw-web_3.0-32.jpg') }}"/>
                    </a>
                </div>
                <div class="swiper-slide">
                    <a href="{{route('frontend.portfolio.show', ['type'=> 15])}}">
                        <img class="portfolio-img" src="{{ asset('images/bnw/portfolio/bnw-web_3.0-33.jpg') }}"/>
                    </a>
                </div>
            </div>
        </div>
        <div class="container">
            --}}{{--Link and info--}}{{--
            <div class="row mt-5">
                <a class="font-av-next let-spa-3 txt-subheader text-white text-left col"
                   href="{{ route('frontend.portfolio') }}">
                    VIEW MORE
                </a>
                <span class="d-block d-md-none font-av-next let-spa-3 txt-subheader text-white text-right col">
                Slide >></span>
            </div>
        </div>
    </section>--}}

    {{--Portfolio Testing--}}
    <section id="works" class="py-5 py-sm-5 pt-sm-5" style="background-color: black;">
        {{--Portfolio Desktop--}}
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="text-white">
                        {{--Title--}}
                        <div>
                            <p class="font-av-next txt-subheader">OUR WORKS</p>
                            <p class="font-didot txt-what">Curated Portfolios</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--Porto Swiper--}}
        <div class="container-fluid mt-md-3">
            <div class="row">
                <div class="col-12">
                    <div class="swiper-container swiper-container-portfolio">
                        <!-- Add Scrollbar -->
                        <div class="swiper-scrollbar swiper-scrollbar-portfolio pr-2"></div>
                        <!-- If we need pagination -->
                        {{--<div class="swiper-pagination swiper-pagination-portfolio"></div>--}}
                        <div class="container swiper-wrapper swiper-wrapper-portfolio{{-- pl-md-5--}} mt-3">
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/trinityland2.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/trinityland2.jpg') }}" alt="Trinity Land"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/bsdcity.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/bsdcity.jpg') }}" alt="BSD City"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/nuansa1.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/nuansa1.jpg') }}" alt="Nuansa Apartment"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/warcorp.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/warcorp.jpg') }}" alt="Warcorp"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/solterra.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/solterra.jpg') }}" alt="Solterra"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/metro.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/metro.jpg') }}" alt="Metro"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/samawa.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/samawa.jpg') }}" alt="Samawa Apartment"/>
                                </a>
                                {{--                            <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/samawa.jpg') }}"--}}
                                {{--                                 id="modal-2"/>--}}
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/thescott.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/thescott.jpg') }}" alt="The Scott"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/gandaland.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/gandaland.jpg') }}" alt="Ganda Land"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/sinarmasland.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/sinarmasland.jpg') }}" alt="Sinarmas Land"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/marcs1.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/marcs1.jpg') }}" alt="Marcs Boulevard"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/marcs2.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/marcs2.jpg') }}" alt="Marcs Boulevard"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/swasana.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/swasana.jpg') }}" alt="Swasana Apartment"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/for.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/for.jpg') }}" alt="F.O.R"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/hype.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/hype.jpg') }}" alt="Hype"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/kingland.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/kingland.jpg') }}" alt="Kingland"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/sananda.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/sananda.jpg') }}" alt="Sananda Apartment"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/salt.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/salt.jpg') }}" alt="Saltventure"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/nuansa2.jpg') }}">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/nuansa2.jpg') }}" alt="Nuansa Apartment" data-fancybox="gallery"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/nyhendrawan1.jpg') }}">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/nyhendrawan1.jpg') }}" alt="Nyonya Hendrawan" data-fancybox="gallery"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/nyhendrawan2.jpg') }}">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/nyhendrawan2.jpg') }}" alt="Nyonya Hendrawan" data-fancybox="gallery"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/revco1.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/revco1.jpg') }}" alt="Revco"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/revco2.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/revco2.jpg') }}" alt="Revco"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/rivia1.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/rivia1.jpg') }}" alt="Rivia"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/rivia2.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/rivia2.jpg') }}" alt="Rivia"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/sanusa.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/sanusa.jpg') }}" alt="Sanusa Apartment"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/rivia3.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/rivia3.jpg') }}" alt="Rivia"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/trinityland1.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/trinityland1.jpg') }}" alt="Trinity Land"/>
                                </a>
                            </div>
                            <div class="swiper-slide">
                                <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/waresix.jpg') }}" data-fancybox="gallery">
                                    <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/waresix.jpg') }}" alt="Waresix"/>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            {{--Link and info--}}
            <div class="row mt-5">
                <div class="col">
                    <a href="{{ route('frontend.portfolio') }}">
                        <p class="font-av-next let-spa-3 txt-subheader text-white text-left">VIEW MORE ></p>
                    </a>
                </div>
                <div class="col">
                    <span class="d-block d-md-none font-av-next let-spa-3 txt-subheader text-white text-right">
                        Slide >>
                    </span>
                </div>
            </div>
        </div>
    </section>

    {{--Services Mobile--}}
    <section id="services" class="text-white pt-3 pb-5 d-block d-md-none" style="background-color: black">
        <div class="container">
            <div>
                <p class="font-av-next txt-subheader">OUR SERVICES</p>
                <p class="font-didot txt-what">What We Do</p>
            </div>
        </div>
        <div class="swiper-container swiper-container-mobile-services">

            <div class="swiper-scrollbar swiper-scrollbar-mobile-services pr-2"></div>

            <div class="swiper-wrapper swiper-wrapper-mobile-services mt-5">

                <div class="swiper-slide">
                    <div class="container">
                        {{--Row 1--}}
                        <div class="row justify-content-center mt-2">
                            <div class="logo-services-wrapper col-3">
                                <img src="{{ asset('images/bnw/home/bnw-web-icons_3.2-40.png') }}"
                                     class="logo-services">
                            </div>
                            <div class="col-7">
                                <p class="txt-services-title">Project Concept</p>
                                <p class="txt-services-body"> Develop an engaging and inspiring branding concept that wins your audience’ attention,<br/>
                                    injecting a unique personality that defines your brand perfectly
                                </p>
                            </div>
                        </div>
                        {{--Row 2--}}
                        <div class="row justify-content-center mt-4">
                            <div class="logo-services-wrapper col-3">
                                <img src="{{ asset('images/bnw/home/bnw-web-icons_3.2-41.png') }}"
                                     class="logo-services">
                            </div>
                            <div class="col-7">
                                <p class="txt-services-title">Logo & Naming</p>
                                <p class="txt-services-body"> Create a memorable brand through a carefully planned logo and<br/>
                                    name that meets your branding needs,<br/>
                                    done on point
                                </p>
                            </div>
                        </div>
                        {{--Row 3--}}
                        <div class="row justify-content-center mt-4">
                            <div class="logo-services-wrapper col-3">
                                <img src="{{ asset('images/bnw/home/bnw-web-icons_3.2-42.png') }}"
                                     class="logo-services">
                            </div>
                            <div class="col-7">
                                <p class="txt-services-title">Promotional Copywriting</p>
                                <p class="txt-services-body"> Let your brand tell its own story,
                                    crafted through the art of storytelling and
                                    engaging words that complements flawlessly
                            </div>
                        </div>
                        {{--Row 4--}}
                        <div class="row justify-content-center mt-4">
                            <div class="logo-services-wrapper col-3">
                                <img src="{{ asset('images/bnw/home/bnw-web-icons_3.2-43.png') }}"
                                     class="logo-services">
                            </div>
                            <div class="col-7">
                                <p class="txt-services-title">Illustrative Design</p>
                                <p class="txt-services-body"> Craft an appealing visualization that defines your brand in its perfect tone,
                                    attracting audience by its thoughtful projection
                            </div>
                        </div>
                    </div>
                </div>

                <div class="swiper-slide">

                    <div class="container">
                        {{--Row 1--}}
                        <div class="row justify-content-center mt-4">
                            <div class="logo-services-wrapper col-3">
                                <img src="{{ asset('images/bnw/home/bnw-web-icons_3.2-44.png') }}"
                                     class="logo-services">
                            </div>
                            <div class="col-7">
                                <p class="txt-services-title">Company Profile Design</p>
                                <p class="txt-services-body"> Present your brand in a layout that portrays maturity and dependability,
                                    improving its value at the best
                                </p>
                            </div>
                        </div>
                        {{--Row 2--}}
                        <div class="row justify-content-center mt-4">
                            <div class="logo-services-wrapper col-3">
                                <img src="{{ asset('images/bnw/home/bnw-web-icons_3.2-45.png') }}"
                                     class="logo-services">
                            </div>
                            <div class="col-7">
                                <p class="txt-services-title">Website Development</p>
                                <p class="txt-services-body"> Showcase your brand digitally in this modern era,
                                    defining a strong position through a well-developed website
                                </p>
                            </div>
                        </div>
                        {{--Row 3--}}
                        <div class="row justify-content-center mt-4">
                            <div class="logo-services-wrapper col-3">
                                <img src="{{ asset('images/bnw/home/bnw-web-icons_3.2-46.png') }}"
                                     class="logo-services">
                            </div>
                            <div class="col-7">
                                <p class="txt-services-title">Marketing Tools Design</p>
                                <p class="txt-services-body"> Invite your audience to feel your brand values through well-designed tools,
                                    developed perfectly for your Marketing needs
                                </p>
                            </div>
                        </div>
                        {{--Row 4--}}
                        <div class="row justify-content-center mt-4">
                            <div class="logo-services-wrapper col-3">
                                <img src="{{ asset('images/bnw/home/bnw-web-icons_3.2-47.png') }}"
                                     class="logo-services">
                            </div>
                            <div class="col-7">
                                <p class="txt-services-title">Stationaries Design</p>
                                <p class="txt-services-body"> Customize your stationaries that creates an exclusive touch to your brand,
                                    a distinct identity that defines your professionalism
                                </p>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

        {{--Link and info--}}
        <div class="mt-3 pl-4 pr-3">
            {{--<a class="font-av-next let-spa-3 txt-subheader text-white float-left" href="{{ route('frontend.portfolio') }}">
                VIEW MORE
            </a>--}}
            <span class="d-block d-md-none font-av-next let-spa-3 txt-subheader text-white float-right">
                Slide >>
            </span>
        </div>
    </section>

    <section id="clients" class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-4 col-sm-3 text-dark">
                    <p class="font-av-next txt-subheader">OUR CLIENTS</p>
                    <span class="font-didot-bold txt-who">Who We Have<br/>
                        Worked With</span>
                </div>
                <div class="col-7 col-sm-8 d-flex align-items-center">
                    <div class="swiper-container swiper-container-clients">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="box-clients">
                                    <div class="h-100 d-flex justify-content-center flex-column text-center">
                                        <img src="{{ asset('images/bnw/home/bnw-web_3.0-19.png') }}" class="img-clients-1"
                                             alt="">
                                    </div>
                                </div>

                            </div>
                            <div class="swiper-slide">
                                <div class="box-clients">
                                    <div class="h-100 d-flex justify-content-center flex-column text-center">
                                        <img src="{{ asset('images/bnw/home/bnw-web_3.0-20.png') }}" class="img-clients-2"
                                             alt="">
                                    </div>
                                </div>

                            </div>
                            <div class="swiper-slide">
                                <div class="box-clients">
                                    <div class="h-100 d-flex justify-content-center flex-column text-center">
                                        <img src="{{ asset('images/bnw/home/bnw-web_3.0-21.png') }}" class="img-clients-3"
                                             alt="">
                                    </div>
                                </div>

                            </div>
                            <div class="swiper-slide">
                                <div class="box-clients">
                                    <div class="h-100 d-flex justify-content-center flex-column text-center">
                                        <img src="{{ asset('images/bnw/home/bnw-web_3.0-22.png') }}" class="img-clients-4"
                                             alt="">
                                    </div>
                                </div>

                            </div>
                            <div class="swiper-slide">
                                <div class="box-clients">
                                    <div class="h-100 d-flex justify-content-center flex-column text-center">
                                        <img src="{{ asset('images/bnw/home/bnw-web_3.0-23.png') }}" class="img-clients-5"
                                             alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="box-clients">
                                    <div class="h-100 d-flex justify-content-center flex-column text-center">
                                        <img src="{{ asset('images/bnw/home/bnw-web_3.0-48.jpg') }}" class="img-clients-6"
                                             alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="box-clients">
                                    <div class="h-100 d-flex justify-content-center flex-column text-center">
                                        <img src="{{ asset('images/bnw/home/bnw-web_3.0-49.jpg') }}" class="img-clients-7"
                                             alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="box-clients">
                                    <div class="h-100 d-flex justify-content-center flex-column text-center">
                                        <img src="{{ asset('images/bnw/home/bnw-web_3.0-50.jpg') }}" class="img-clients-6"
                                             alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="box-clients">
                                    <div class="h-100 d-flex justify-content-center flex-column text-center">
                                        <img src="{{ asset('images/bnw/home/bnw-web_3.0-51.jpg') }}" class="img-clients-6"
                                             alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="box-clients">
                                    <div class="h-100 d-flex justify-content-center flex-column text-center">
                                        <img src="{{ asset('images/bnw/home/bnw-web_3.0-52.jpg') }}" class="img-clients-10"
                                             alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="box-clients">
                                    <div class="h-100 d-flex justify-content-center flex-column text-center">
                                        <img src="{{ asset('images/bnw/home/bnw-web_3.0-53.jpg') }}" class="img-clients-8"
                                             alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="box-clients">
                                    <div class="h-100 d-flex justify-content-center flex-column text-center">
                                        <img src="{{ asset('images/bnw/home/bnw-web_3.0-54.jpg') }}" class="img-clients-9"
                                             alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="box-clients">
                                    <div class="h-100 d-flex justify-content-center flex-column text-center">
                                        <img src="{{ asset('images/bnw/home/bnw-web_3.0-55.jpg') }}" class="img-clients-8"
                                             alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="box-clients">
                                    <div class="h-100 d-flex justify-content-center flex-column text-center">
                                        <img src="{{ asset('images/bnw/home/bnw-web_3.0-56.jpg') }}" class="img-clients-8"
                                             alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="box-clients">
                                    <div class="h-100 d-flex justify-content-center flex-column text-center">
                                        <img src="{{ asset('images/bnw/home/bnw-web_3.0-57.jpg') }}" class="img-clients-6"
                                             alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="box-clients">
                                    <div class="h-100 d-flex justify-content-center flex-column text-center">
                                        <img src="{{ asset('images/bnw/home/bnw-web_3.0-58.jpg') }}" class="img-clients-8"
                                             alt="">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-1 center-vertical-horizontal">
                    <i class="fa fa-chevron-right swiper-container-clients-next"></i>
                </div>
            </div>
        </div>
    </section>

@endsection
@section('styles')
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css" type="text/css" media="screen" />
    <style>
        /*.fancybox-viewer img{*/
        /*    width: 200px;*/
        /*    height: auto;*/
        /*}*/

        .slick-next {
            right: 5px;
        }
        .slick-prev {
            left: 5px;
            z-index: 100;
        }

        .menu-mobile .main-menu > li > a{
            font-size: 14px;
        }

        .pt-percent-banner{
            padding-top: 44%;
        }

        .ml-em-banner{
            margin-left: 2.2em;
        }

        .center-vertical-horizontal {
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .fa-chevron-right-white {
            color: white;
        }

        .bnw_banner_background{
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
            height: 100vh;
        }

        .bnw_banner_1{
            background-image: url('{{ asset('images/bnw/home/bnw_banner_mobile_1.jpg') }}');
        }

        .bnw_banner_2{
            background-image: url('{{ asset('images/bnw/home/bnw_banner_mobile_2.jpg') }}');
        }

        .bnw_banner_3{
            background-image: url('{{ asset('images/bnw/home/bnw_banner_mobile_3.jpg') }}');
        }

        .swiper-container {
            display: flex;
            width: 100%;
            box-sizing: border-box;
        }

        .swiper-wrapper {
            margin-left: auto;
            margin-right: auto;
            box-sizing: border-box;
        }

        .swiper-slide {
            width: auto;
            flex-wrap: nowrap;
        }

        .swiper-wrapper-mobile-services .swiper-slide {
            width: 100%;
            text-align: center;
        }

        .swiper-wrapper-portfolio {
            margin-top: 15px;
        }

        /*.swiper-wrapper-portfolio > .swiper-slide {
            display: inline-flex;
        }*/

        .swiper-wrapper-portfolio .swiper-slide:first-child {
            /*margin-left: 15px;*/
        }

        .swiper-wrapper-portfolio .swiper-slide:last-child {
            /*margin-right: 15px;*/
        }

        .swiper-container-horizontal > .swiper-scrollbar {
            position: absolute;
            left: 88%;
            bottom: 3px;
            z-index: 50;
            height: 3px;
            width: 10%;
            top: 0;
        }

        #services .swiper-container-horizontal > .swiper-scrollbar{
            left: 77%;
        }

        .swiper-scrollbar {
            border-radius: 10px;
            position: relative;
            -ms-touch-action: none;
            background: grey;
        }

        .swiper-scrollbar-mobile-services {
            border-radius: 10px;
            position: relative;
            -ms-touch-action: none;
            background: grey;
        }

        .swiper-scrollbar-drag {
            height: 3px;
            width: 100%;
            position: relative;
            background: darkgreen;
            border-radius: 10px;
            left: 0;
            top: 0;
        }

        .border-custom-green {
            border-bottom: 1px solid darkgreen;
            width: 120px;
            top: 15px;
            position: relative;
        }

        .box-clients{
            height: 5em;
            width: 10em;
        }

        .img-clients-1 {
            width: 80%;
            height: auto;
            /*margin-top: 15px;*/
            /*padding-top: 10px;*/
        }

        .img-clients-2 {
            width: 70%;
            height: auto;
            /*margin-top: 3px;*/
        }

        .img-clients-3 {
            width: 100%;
            height: auto;
            /*margin-top: 13px;*/

        }

        .img-clients-4 {
            width: 65%;
            height: auto;
            /*margin-top: 0;*/
            /*margin-left: 10px;*/
        }

        .img-clients-5 {
            width: 80%;
            height: auto;
            /*margin-top: 18px;*/
        }

        .img-clients-6 {
            width: 90%;
            height: auto;
        }
        .img-clients-7 {
            width: 90%;
            height: auto;
        }
        .img-clients-8 {
            width: 90%;
            height: auto;
        }

        .img-clients-9 {
            width: 90%;
            height: auto;
        }
        .img-clients-10 {
            width: 90%;
            height: auto;
        }

        .logo-services-wrapper {
            display: -webkit-flex;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-align-content: center;
            -ms-flex-align: center;
            -webkit-align-items: center;
            align-items: center;
            justify-content: center;
        }

        .logo-services {
            max-height: 55px;
        }

        .txt-header-main {
            font-size: 44px;
            line-height: 1em;
        }

        .dropdown-services {
            padding-left: 80px;
        }

        .dropdown-height-1 {
            height: 25px;
            margin-left: 15px;
        }

        .dropdown-height {
            height: 260px;
        }

        .border-services {
            height: 200px;
            padding-left: 6px;
        }

        .scrolling-wrapper-flexbox {
            display: flex;
            flex-wrap: nowrap;
            overflow-x: auto;
            padding-bottom: 25px;
        }

        .card-box {
            display: flex;
            flex-wrap: nowrap;
            overflow-x: auto;
            flex: 0 0 auto;
            padding: 0 3px;
        }

        .swiper-slide .portfolio-img {
            max-width: 100%;
            height: auto;
        }

        .img-services {
            width: 100%;
        }

        .scrolling-wrapper-flexbox::-webkit-scrollbar {
            -webkit-appearance: none;
        }

        .scrolling-wrapper-flexbox::-webkit-scrollbar:vertical {
            width: 5px;
        }

        .scrolling-wrapper-flexbox::-webkit-scrollbar:horizontal {
            height: 5px;
        }

        .scrolling-wrapper-flexbox::-webkit-scrollbar-thumb {
            border-radius: 4px;
            border: 1px solid white;
            /* should match background, can't be transparent */
            background-color: #fff;
        }

        @media (min-width: 400px) {
            .border-custom-green {
                border-bottom: 1px solid darkgreen;
                width: 120px;
                top: 15px;
                position: relative;
            }

            .header-img {
                /*height: 600px !important;*/
                margin-top: -70px;
            }

            /*.swiper-slide .portfolio-img {
                max-height: 250px;
                width: auto;
            }*/
        }

        @media (min-width: 576px) {

            /*.swiper-slide .portfolio-img {
                width: auto;
                min-height: 309px;
            }*/
            .details-margs {
                margin-left: 15px;
                position: relative;
                top: -8px;
            }

            .dropdown-services {
                padding-left: 0px;
            }

            .txt-header-contact {
                font-size: 26px;
                margin-bottom: 50px;
                text-align: left !important;
            }

            .header-img {
                /*height: 600px !important;*/
                margin-top: -70px;
            }

            .beproud {
                padding-left: 35px;
                padding-right: 35px;
                padding-top: 50px !important;
            }

            .txt-proud {
                font-size: 13px;
            }
        }

        @media (min-width: 768px) {
            .ml-em-banner{
                margin-left: 7em;
            }

            .img-clients-1 {
                height: 38px;
                margin-top: 15px;
                padding-top: 10px;
            }

            .img-clients-2 {
                height: 70px;
                margin-top: 3px;
            }

            .img-clients-3 {
                height: 55px;
                margin-top: 13px;
            }

            .img-clients-4 {
                height: 41px;
                margin-top: 0;
                margin-left: 10px;
            }

            .img-clients-5 {
                height: 27px;
                margin-top: 18px;
            }
            .img-clients-6 {
                /*height: 50px;*/
                margin-top: 20px;
            }
            /*.img-clients-7 {*/
            /*    height: 50px;*/
            /*    margin-top: 0px;*/
            /*}*/
            .img-clients-8 {
                /*height: 65px;*/
                margin-top: 20px;
            }

            .img-clients-9 {
                /*height: 63px;*/
                margin-top: 20px;
            }

            .img-clients-10 {
                /*height: 58px;*/
                margin-top: 20px;
            }

            .slick-next:before, .slick-prev:before{
                font-size: 50px;
            }
            .slick-next {
                right: 40px;
            }
            .slick-prev {
                left: 20px;
                z-index: 100;
            }
            .pt-percent-banner{
                padding-top: 10%;
            }

            .bnw_banner_background{
                height: 100vh;
            }

            .bnw_banner_1{
                background-image: url('{{ asset('images/bnw/home/bnw_banner_1.jpg') }}');
            }

            .bnw_banner_2{
                background-image: url('{{ asset('images/bnw/home/bnw_banner_2.jpg') }}');
            }

            .bnw_banner_3{
                background-image: url('{{ asset('images/bnw/home/bnw_banner_3.jpg') }}');
            }

            .border-custom-green {
                border-bottom: 1px solid darkgreen;
                width: 135px;
                top: 15px;
                position: relative;
            }

            /*.swiper-slide .portfolio-img {
                width: auto;
                min-height: 15px;
            }*/

            .txt-header-main {
                font-size: 78px;
                padding-top: 0px;
            }

            .dropdown-services {
                padding-left: 0px;
            }

            .img-services {
                width: 390px;
            }

            .img-bnw {
                width: 100px;
                padding-bottom: 30px;
            }

            .txt-banner-spacing {
                padding-top: 10%;
            }

            .spacing-home-1 {
                padding-top: 100px;
                padding-bottom: 100px;
            }

            .txt-header-contact {
                font-size: 30px;
                text-align: left;
                margin-bottom: 50px;
            }

            .txt-header-about {
                font-size: 30px;
                margin-bottom: 10px;

            }
        }

        @media (min-width: 992px) {
            /*.swiper-slide .portfolio-img {
                width: auto;
                min-height: 365px;
            }*/

            .bnw_banner_background{
                height: 100vh;
            }
        }

        @media (min-width: 1200px) {
            /*.swiper-slide .portfolio-img {
                width: auto;
                min-height: 475px;
            }*/
            .header-img {
                /*min-height: 500px;*/
            }
        }

        @media (min-width: 1400px) {
            /*.swiper-slide .portfolio-img {
                width: auto;
                min-height: 500px;
            }*/
            .header-img {
                /*min-height: 500px;*/
            }
        }

        @media (min-width: 1600px) {
            /*.swiper-slide .portfolio-img {
                width: 100%;
                height: auto;
            }*/
            .header-img {
                /*min-height: 500px;*/
            }
        }
    </style>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.js"></script>
    <script>
        $("a.fancybox-viewer").fancybox({
            clickContent: "close"
        });

        $('#bnw_banner').slick({
            dots: false,
            arrows: true,
            draggable: true,
            infinite: true,
            autoplay: false,
            autoplaySpeed: 2500,
            slidesToShow: 1,
            slidesToScroll: 1
        });

        // $(".five-slider").slick({
        //     dots: false,
        //     infinite: false,
        //     slidesToShow: 5,
        //     slidesToScroll: 1,
        //     arrows: false,
        //     responsive: [
        //         {
        //             breakpoint: 768,
        //             settings: {
        //                 slidesToShow: 2,
        //                 slidesToScroll: 1,
        //                 infinite: false,
        //                 dots: false,
        //                 arrows: false,
        //             },
        //         }
        //     ]
        // });

        /*Clients Swiper
        var clientsSwiper = new Swiper('.swiper-container-portfolio', {
            slidesPerView: 3,
            spaceBetween: 10,
            scrollbar: {
                el: '.swiper-scrollbar-portfolio',
                hide: false,
            },
            breakpoints: {
                768: {
                    slidesPerView: 6,
                    spaceBetween: 10,
                },
            }
        });*/

        /*Portfolio Swiper*/
        var portfolioSwiper = new Swiper('.swiper-container-portfolio', {
            slidesPerView: 3,
            spaceBetween: 5,
            observer: true,
            observeParents: true,
            freeMode: true,
            freeModeMomentum: true,
            /*grabCursor: true,*/
            /*pagination: {
                el: '.swiper-pagination-portfolio',
                type: 'bullets',
            },*/
            scrollbar: {
                el: '.swiper-scrollbar-portfolio',
                hide: false,
                draggable: true,
            },
            /*mousewheel: {
                invert: false,
                releaseOnEdges: true,
                mousewheelSensitivity: 0,
            },*/
            breakpoints: {
                768: {
                    slidesPerView: 6,
                    spaceBetween: 5,
                },
            }
        });

        /*Desktop Header Swiper*/
        var swiperHeaderDesktop = new Swiper('.swiper-container-desktop-header', {
            slidesPerView: 'auto',
            spaceBetween: 20,
            observer: true,
            observeParents: true
        });

        /*Mobile Header Swiper*/
        var swiperHeaderMobile = new Swiper('.swiper-container-mobile-header', {
            slidesPerView: 1,
            spaceBetween: 10,
            navigation: {
                nextEl: '.swiper-container-mobile-header-next',
            },
        });

        /*Mobile Services Swiper*/
        var swiperServicesMobile = new Swiper('.swiper-container-mobile-services', {
            slidesPerView: 1,
            spaceBetween: 10,
            centeredSlides: true,
            scrollbar: {
                el: '.swiper-scrollbar-mobile-services',
                hide: false,
                draggable: true,
            },
            breakpoints: {
                768: {
                    slidesPerView: 6,
                    spaceBetween: 10,
                },
            }
        });

        /*Clients Swiper*/
        var swiperClient = new Swiper('.swiper-container-clients', {
            slidesPerView: 2,
            spaceBetween: 140,
            freeMode: true,
            freeModeMomentum: true,
            loop: true,
            /*mousewheel: {
                invert: false,
                releaseOnEdges: true,
            },*/
            breakpoints: {
                768: {
                    slidesPerView: 'auto',
                    spaceBetween: 20,
                },
                992: {
                    slidesPerView: 5,
                    spaceBetween: 20,
                },
            },
            navigation: {
                nextEl: '.swiper-container-clients-next',
            },
        });

        // Yg buat terbang2
        function navigateSection(section) {
            var offsetTop = $('#' + section).offset().top;

            if (offsetTop) {
                $('html,body').animate({
                    scrollTop: offsetTop
                }, 1000);

                return false;
            }
        }

    </script>

@endsection
