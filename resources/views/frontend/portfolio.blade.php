@extends('layouts.frontend-v2')

@section('head_and_title')
<meta name="description" content="BNW Portfolio">
<meta name="author" content="PT. Generasi Muda Gigih">
<meta name="keywords"
    content="Ben and Wyatt, Branding House, Design House, Office, Residential, Apartment, Commercial, Spaces">

<title>BNW - PORTFOLIO</title>
@endsection

@section('content')
<section id="portfolio" style="background-color: black;margin-top:-100px;padding-top:100px;padding-bottom:75px;">
    <div class="container">
        <div class="row text-white pt-5">
            <div class="col-md-2 col-12">
                <div class="d-none d-md-block">
                    <p class="font-av-next txt-subheader">WORKS</p>
                    <span class="font-didot txt-what">Curated<br/>
                        Portfolios</span>
                </div>
                <div class="d-block d-md-none pb-md-0 pb-3">
                    <p class="font-av-next txt-subheader">OUR WORKS</p>
                    <span class="font-didot txt-what">Curated Portfolios</span>
                </div>
            </div>
            <div class="col-md-10">
                <div class="slick-wrapper">
                    <div class="slick1">
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/nuansa1.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/nuansa1.jpg') }}" alt="Nuansa Apartment"/>
                            </a>
                        </div>
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/metro.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/metro.jpg') }}" alt="Metro"/>
                            </a>
                        </div>

                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/warcorp.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/warcorp.jpg') }}" alt="Warcorp"/>
                            </a>
                        </div>

                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/salt.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/salt.jpg') }}" alt="Saltventure"/>
                            </a>
                        </div>
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/sananda.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/sananda.jpg') }}" alt="Sananda Apartment"/>
                            </a>
                        </div>
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/thescott.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/thescott.jpg') }}" alt="The Scott"/>
                            </a>
                        </div>
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/gandaland.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/gandaland.jpg') }}" alt="Ganda Land"/>
                            </a>
                        </div>
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/sinarmasland.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/sinarmasland.jpg') }}" alt="Sinarmas Land"/>
                            </a>
                        </div>
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/bsdcity.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/bsdcity.jpg') }}" alt="BSD City"/>
                            </a>
                        </div>
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/marcs1.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/marcs1.jpg') }}" alt="Marcs Boulevard"/>
                            </a>
                        </div>
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/marcs2.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/marcs2.jpg') }}" alt="Marcs Boulevard"/>
                            </a>
                        </div>
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/trinityland1.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/trinityland1.jpg') }}" alt="Trinity Land"/>
                            </a>
                        </div>
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/solterra.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/solterra.jpg') }}" alt="Solterra"/>
                            </a>
                        </div>
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/for.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/for.jpg') }}" alt="F.O.R"/>
                            </a>
                        </div>
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/hype.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/hype.jpg') }}" alt="Hype"/>
                            </a>
                        </div>
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/rivia2.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/rivia2.jpg') }}" alt="Rivia"/>
                            </a>
                        </div>
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/kingland.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/kingland.jpg') }}" alt="Kingland"/>
                            </a>
                        </div>
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/nuansa2.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/nuansa2.jpg') }}" alt="Nuansa Apartment"/>
                            </a>
                        </div>
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/swasana.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/swasana.jpg') }}" alt="Swasana Apartment"/>
                            </a>
                        </div>
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/nyhendrawan1.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/nyhendrawan1.jpg') }}" alt="Nyonya Hendrawan"/>
                            </a>
                        </div>
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/nyhendrawan2.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/nyhendrawan2.jpg') }}" alt="Nyonya Hendrawan"/>
                            </a>
                        </div>
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/revco1.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/revco1.jpg') }}" alt="Revco"/>
                            </a>
                        </div>
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/sanusa.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/sanusa.jpg') }}" alt="Sanusa Apartment"/>
                            </a>
                        </div>
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/revco2.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/revco2.jpg') }}" alt="Revco"/>
                            </a>
                        </div>
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/rivia1.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/rivia1.jpg') }}" alt="Rivia"/>
                            </a>
                        </div>
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/rivia3.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/rivia3.jpg') }}" alt="Rivia"/>
                            </a>
                        </div>

                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/trinityland2.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/trinityland2.jpg') }}" alt="Trinity Land"/>
                            </a>
                        </div>
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/samawa.jpg') }}" data-fancybox="gallery">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/samawa.jpg') }}" alt="Samawa Apartment"/>
                            </a>
                            {{--                                <img class="portfolio-img modal-port-2" src="{{ asset('images/bnw/portfolio/bnw-web_3.1-14.jpg') }}"/>--}}
                        </div>
                        <div class="slide-item">
                            <a class="fancybox-viewer" rel="portfolio_live" href="{{ asset('images/bnw/portfolio_live/waresix.jpg') }}">
                                <img class="portfolio-img" src="{{ asset('images/bnw/portfolio_live/waresix.jpg') }}" alt="Waresix" data-fancybox="gallery"/>
                            </a>
                        </div>
                    </div>
                </div>
{{--                <!-- Swiper -->--}}
{{--                <div class="swiper-container">--}}
{{--                    <div class="swiper-wrapper">--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-13.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-14.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-15.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-16.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-17.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-18.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-19.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-20.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-27.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-28.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-29.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-30.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-31.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-32.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-33.jpg') }}" /></div>--}}

{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-13.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-14.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-15.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-16.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-17.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-18.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-19.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-20.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-27.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-28.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-29.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-30.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-31.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-32.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-33.jpg') }}" /></div>--}}

{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-13.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-14.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-15.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-16.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-17.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-18.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-19.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-20.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-27.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-28.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-29.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-30.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-31.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-32.jpg') }}" /></div>--}}
{{--                        <div class="slide-item"><img src="{{ asset('images/bnw/portfolio/bnw-web_3.0-33.jpg') }}" /></div>--}}
{{--                    </div>--}}
{{--                    <!-- Add Pagination -->--}}
{{--                    <div class="swiper-pagination"></div>--}}
                </div>
            </div>
        </div>
</section>
{{--<section class="py-5">--}}
{{--    <div class="container">--}}
{{--        <div class="row">--}}
{{--            <div class="col-md-3 col-4 text-dark">--}}
{{--                <p class="font-av-next txt-subheader">OUR CLIENTS</p>--}}
{{--                <p class="font-didot txt-who">Who We Have<br/>--}}
{{--                    Worked With</p>--}}
{{--            </div>--}}
{{--            <div class="col-md-9 col-8">--}}
{{--                <div class="five-sliders align-content-center">--}}
{{--                    <div class="text-center">--}}
{{--                        <img src="{{ asset('images/bnw/home/bnw-web_3.0-19.png') }}" class="img-clients-1" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="text-center">--}}
{{--                        <img src="{{ asset('images/bnw/home/bnw-web_3.0-20.png') }}" class="img-clients-2" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="text-center">--}}
{{--                        <img src="{{ asset('images/bnw/home/bnw-web_3.0-21.png') }}" class="img-clients-3" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="text-center">--}}
{{--                        <img src="{{ asset('images/bnw/home/bnw-web_3.0-22.png') }}" class="img-clients-4" alt="">--}}
{{--                    </div>--}}
{{--                    <div class="text-center">--}}
{{--                        <img src="{{ asset('images/bnw/home/bnw-web_3.0-23.png') }}" class="img-clients-5" alt="">--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</section>--}}
<section id="clients" class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-4 col-sm-3 text-dark">
                <p class="font-av-next txt-subheader">OUR CLIENTS</p>
                <span class="font-didot-bold txt-who">Who We Have<br/>
                        Worked With</span>
            </div>
            <div class="col-7 col-sm-8 d-flex align-items-center">
                <div class="swiper-container swiper-container-clients">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide">
                            <div class="box-clients">
                                <div class="h-100 d-flex justify-content-center flex-column text-center">
                                    <img src="{{ asset('images/bnw/home/bnw-web_3.0-19.png') }}" class="img-clients-1"
                                         alt="">
                                </div>
                            </div>

                        </div>
                        <div class="swiper-slide">
                            <div class="box-clients">
                                <div class="h-100 d-flex justify-content-center flex-column text-center">
                                    <img src="{{ asset('images/bnw/home/bnw-web_3.0-20.png') }}" class="img-clients-2"
                                         alt="">
                                </div>
                            </div>

                        </div>
                        <div class="swiper-slide">
                            <div class="box-clients">
                                <div class="h-100 d-flex justify-content-center flex-column text-center">
                                    <img src="{{ asset('images/bnw/home/bnw-web_3.0-21.png') }}" class="img-clients-3"
                                         alt="">
                                </div>
                            </div>

                        </div>
                        <div class="swiper-slide">
                            <div class="box-clients">
                                <div class="h-100 d-flex justify-content-center flex-column text-center">
                                    <img src="{{ asset('images/bnw/home/bnw-web_3.0-22.png') }}" class="img-clients-4"
                                         alt="">
                                </div>
                            </div>

                        </div>
                        <div class="swiper-slide">
                            <div class="box-clients">
                                <div class="h-100 d-flex justify-content-center flex-column text-center">
                                    <img src="{{ asset('images/bnw/home/bnw-web_3.0-23.png') }}" class="img-clients-5"
                                         alt="">
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="box-clients">
                                <div class="h-100 d-flex justify-content-center flex-column text-center">
                                    <img src="{{ asset('images/bnw/home/bnw-web_3.0-48.jpg') }}" class="img-clients-6"
                                         alt="">
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="box-clients">
                                <div class="h-100 d-flex justify-content-center flex-column text-center">
                                    <img src="{{ asset('images/bnw/home/bnw-web_3.0-49.jpg') }}" class="img-clients-7"
                                         alt="">
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="box-clients">
                                <div class="h-100 d-flex justify-content-center flex-column text-center">
                                    <img src="{{ asset('images/bnw/home/bnw-web_3.0-50.jpg') }}" class="img-clients-6"
                                         alt="">
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="box-clients">
                                <div class="h-100 d-flex justify-content-center flex-column text-center">
                                    <img src="{{ asset('images/bnw/home/bnw-web_3.0-51.jpg') }}" class="img-clients-6"
                                         alt="">
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="box-clients">
                                <div class="h-100 d-flex justify-content-center flex-column text-center">
                                    <img src="{{ asset('images/bnw/home/bnw-web_3.0-52.jpg') }}" class="img-clients-10"
                                         alt="">
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="box-clients">
                                <div class="h-100 d-flex justify-content-center flex-column text-center">
                                    <img src="{{ asset('images/bnw/home/bnw-web_3.0-53.jpg') }}" class="img-clients-8"
                                         alt="">
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="box-clients">
                                <div class="h-100 d-flex justify-content-center flex-column text-center">
                                    <img src="{{ asset('images/bnw/home/bnw-web_3.0-54.jpg') }}" class="img-clients-9"
                                         alt="">
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="box-clients">
                                <div class="h-100 d-flex justify-content-center flex-column text-center">
                                    <img src="{{ asset('images/bnw/home/bnw-web_3.0-55.jpg') }}" class="img-clients-8"
                                         alt="">
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="box-clients">
                                <div class="h-100 d-flex justify-content-center flex-column text-center">
                                    <img src="{{ asset('images/bnw/home/bnw-web_3.0-56.jpg') }}" class="img-clients-8"
                                         alt="">
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="box-clients">
                                <div class="h-100 d-flex justify-content-center flex-column text-center">
                                    <img src="{{ asset('images/bnw/home/bnw-web_3.0-57.jpg') }}" class="img-clients-6"
                                         alt="">
                                </div>
                            </div>
                        </div>
                        <div class="swiper-slide">
                            <div class="box-clients">
                                <div class="h-100 d-flex justify-content-center flex-column text-center">
                                    <img src="{{ asset('images/bnw/home/bnw-web_3.0-58.jpg') }}" class="img-clients-8"
                                         alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-1 center-vertical-horizontal">
                <i class="fa fa-chevron-right swiper-container-clients-next"></i>
            </div>
        </div>
    </div>
</section>
@endsection
@section('styles')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.css"/>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css" type="text/css" media="screen" />

    <style>

        /*.img-clients-1{*/
        /*    height: 30px;*/
        /*    margin-top: 15px;*/
        /*    padding-top:10px;*/
        /*}*/
        /*.img-clients-2{*/
        /*    height: 50px;*/
        /*    margin-top:3px;*/
        /*}*/
        /*.img-clients-3{*/
        /*    height: 40px;*/
        /*    margin-top:13px;*/

        /*}*/
        /*.img-clients-4{*/
        /*    height: 43px;*/
        /*    margin-top: 0px;*/
        /*    margin-left:10px;*/
        /*}*/
        /*.img-clients-5{*/
        /*    height: 27px;*/
        /*    margin-top: 18px;*/

        /*}*/

        #portfolio .swiper-container {
            width: 100%;
            height: 100%;
            margin-left: auto;
            margin-right: auto;
        }

        #clients .swiper-container {
            display: flex;
            width: 100%;
            box-sizing: border-box;
        }

        #clients .swiper-wrapper {
            margin-left: auto;
            margin-right: auto;
            box-sizing: border-box;
        }

        #clients .swiper-slide {
            width: auto;
            flex-wrap: nowrap;
        }

        #clients .swiper-wrapper-mobile-services .swiper-slide {
            width: 100%;
            text-align: center;
        }

        #clients .swiper-wrapper-portfolio .swiper-slide:first-child {
            /*margin-left: 15px;*/
        }

        #clients .swiper-wrapper-portfolio .swiper-slide:last-child {
            /*margin-right: 15px;*/
        }

        #clients .swiper-container-horizontal > .swiper-scrollbar {
            position: absolute;
            left: 88%;
            bottom: 3px;
            z-index: 50;
            height: 3px;
            width: 10%;
            top: 0;
        }

        #clients .swiper-scrollbar {
            border-radius: 10px;
            position: relative;
            -ms-touch-action: none;
            background: grey;
        }

        #clients .swiper-scrollbar-mobile-services {
            border-radius: 10px;
            position: relative;
            -ms-touch-action: none;
            background: grey;
        }

        #clients .swiper-scrollbar-drag {
            height: 100%;
            width: 100%;
            position: relative;
            background: darkgreen;
            border-radius: 10px;
            left: 0;
            top: 0;
        }

        #clients .center-vertical-horizontal {
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .box-clients{
            height: 5em;
            width: 10em;
        }

        .img-clients-1 {
            width: 80%;
            height: auto;
            /*margin-top: 15px;*/
            /*padding-top: 10px;*/
        }

        .img-clients-2 {
            width: 70%;
            height: auto;
            /*margin-top: 3px;*/
        }

        .img-clients-3 {
            width: 100%;
            height: auto;
            /*margin-top: 13px;*/

        }

        .img-clients-4 {
            width: 65%;
            height: auto;
            /*margin-top: 0;*/
            /*margin-left: 10px;*/
        }

        .img-clients-5 {
            width: 80%;
            height: auto;
            /*margin-top: 18px;*/
        }

        .img-clients-6 {
            width: 90%;
            height: auto;
        }
        .img-clients-7 {
            width: 90%;
            height: auto;
        }
        .img-clients-8 {
            width: 90%;
            height: auto;
        }

        .img-clients-9 {
            width: 90%;
            height: auto;
        }
        .img-clients-10 {
            width: 90%;
            height: auto;
        }

        .slide-item {
            text-align: center;
            font-size: 18px;
            /*background: #fff;*/
            /*height: calc((100% - 30px) / 2);*/

            /* Center slide text vertically */
            display: -webkit-box;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: inline-flex;
            -webkit-box-pack: center;
            -ms-flex-pack: center;
            -webkit-justify-content: center;
            justify-content: center;
            -webkit-box-align: center;
            -ms-flex-align: center;
            -webkit-align-items: center;
            align-items: center;
        }


    .slick-dots li.slick-active a{
        font-size: 16px;
        font-weight: bold;
        color: white;
    }


    .slick-dots li a{
        font-size: 14px;
        color: white;
    }

    .slick-dots{
        bottom:-50px;
    }
    .slide-item img{
        display: flex;
        height: 220px;
        width:auto;
    }

    .header-img {
        min-height: 200px !important;
    }

    .txt-subheader-contact {
        font-size: 18px;
    }

    .btn-paulmarc-more .w-25 {
        width: 75% !important;
    }

    .txt-header-contact {
        font-size: 22px;
        text-align: left;
    }

    .txt-banner-spacing {
        padding-top: 15%;
    }

    .img-portfolio {
        /* width: 600px; */
        width: 100%;
    }

    /* PAGINATION LINK OVERRIDE */
    .page-item.active .page-link {
        border: 2px solid #fff !important;
        background-color: rgb(26, 30, 40) !important;
        border-radius: 20px;
        margin-right: 2px;
        margin-left: 2px;
    }

    .page-link {
        font-size: 18px;
        color: #fff;
        border: none !important;
        background-color: rgb(26, 30, 40) !important;
        margin-right: 2px;
        margin-left: 2px;
        padding: 0.25rem 0.75rem;
        line-height: 1.5;
    }


    @media (min-width: 400px) {
        .slide-item img{
            display: flex;
            height: 240px;
            width:auto;
        }
    }

        @media (min-width: 576px) {
            .modal-dialog {
                max-width: 255px;
                margin: 50px auto;
                z-index: 99;
            }

        }

    @media (min-width: 768px) {
        /*.img-clients-1{*/
        /*    height: 30px;*/
        /*    margin-top: 15px;*/
        /*}*/
        /*.img-clients-2{*/
        /*    height: 50px;*/
        /*}*/
        /*.img-clients-3{*/
        /*    height: 60px;*/
        /*    margin-left: -40px;*/
        /*    margin-top: -5px;*/
        /*}*/
        /*.img-clients-4{*/
        /*    height: 50px;*/
        /*    margin-top: -8px;*/
        /*}*/
        /*.img-clients-5{*/
        /*    height: 40px;*/
        /*    margin-top: 10px;*/
        /*    margin-left: -15px;*/
        /*}*/

        .img-clients-1 {
            height: 38px;
            margin-top: 15px;
            padding-top: 10px;
        }

        .img-clients-2 {
            height: 70px;
            margin-top: 3px;
        }

        .img-clients-3 {
            height: 55px;
            margin-top: 13px;
        }

        .img-clients-4 {
            height: 41px;
            margin-top: 0;
            margin-left: 10px;
        }

        .img-clients-5 {
            height: 27px;
            margin-top: 18px;
        }
        .img-clients-6 {
            /*height: 50px;*/
            margin-top: 20px;
        }
        /*.img-clients-7 {*/
        /*    height: 50px;*/
        /*    margin-top: 0px;*/
        /*}*/
        .img-clients-8 {
            /*height: 65px;*/
            margin-top: 20px;
        }

        .img-clients-9 {
            /*height: 63px;*/
            margin-top: 20px;
        }

        .img-clients-10 {
            /*height: 58px;*/
            margin-top: 20px;
        }

        .slide-item img{
            display: flex;
            height: 375px;
            width:auto;
        }
        .img-about {
            max-width: 550px;
        }

        .img-bnw {
            width: 100px;
        }

        .txt-banner-spacing {
            padding-top: 10%;
        }

        .img-portfolio {
            /* width: 600px; */
            width: 100%;
        }

        .padding-portfolio {
            padding-top: 4%;
            padding-left: 50px;
        }

        .header-img {
            min-height: 361px !important;
        }

        .btn-paulmarc-more .w-25 {
            width: 25% !important;
        }

        .txt-header-contact {
            font-size: 30px;
            text-align: left;
            margin-bottom: 50px;
        }

        .txt-header-about {
            font-size: 30px;
            margin-bottom: 10px;

        }
    }

    @media (min-width: 992px) {}

    @media (min-width: 1200px) {
        .header-img {
            min-height: 500px;
        }
    }
    @media (min-width: 1400px) {
        .modal-dialog {
            max-width: 350px;
            margin: 100px auto;
        }
    }
    @media (min-width: 1900px) {
        .modal-dialog {
            max-width: 420px;
            margin: 100px auto;
        }
    }
</style>
@endsection

@section('scripts')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.js"></script>
    <script>
        $("a.fancybox-viewer").fancybox({
            clickContent: "close"
        });

        // $('.slick1').slick({
        //     rows: 5,
        //     dots: true,
        //     arrows: false,
        //     infinite: false,
        //     speed: 300,
        //     slidesToShow: 5,
        //     slidesToScroll: 5
        // });

        $(document).ready(function() {
            var slickOpts = {
                rows: 5,
                arrows: false,
                infinite: false,
                slidesToShow: 5,
                slidesToScroll: 5,
                //centerMode: true,
                easing: 'swing', // see http://api.jquery.com/animate/
                speed: 300,
                dots: true,
                customPaging: function(slick,index) {
                    return '<a>' + (index + 1) + '</a>';
                },
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            rows:4,
                            slidesToShow: 3,
                            slidesToScroll: 3,
                            infinite: false,
                            dots: true
                        },
                        customPaging: function(slick,index) {
                            return '<a>' + (index + 1) + '</a>';
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]

            };
            // Init slick carousel
            $('.slick1').slick(slickOpts);
        });

        $('.slick1').on('setPosition', function(event, slick, direction){
            $.fancybox.destroy();
            $("a.fancybox-viewer").fancybox({
                clickContent: "close"
            });
        });

        /*Clients Swiper*/
        var swiperClient = new Swiper('.swiper-container-clients', {
            slidesPerView: 2,
            spaceBetween: 130,
            freeMode: true,
            freeModeMomentum: true,
            loop: true,
            /*mousewheel: {
                invert: false,
                releaseOnEdges: true,
            },*/
            breakpoints: {
                768: {
                    slidesPerView: 'auto',
                    spaceBetween: 20,
                },
                992: {
                    slidesPerView: 5,
                    spaceBetween: 20,
                },
            },
            navigation: {
                nextEl: '.swiper-container-clients-next',
            },
        });


        // var swiper = new Swiper('.swiper-container', {
        //     slidesPerView: 5,
        //     slidesPerColumn: 5,
        //     spaceBetween: 5,
        //     pagination: {
        //         el: '.swiper-pagination',
        //         clickable: true,
        //         renderBullet: function (index, className) {
        //             return '<span class="' + className + '">' + (index + 1) + '</span>';
        //         },
        //     },
        // });

        // $(".five-sliders").slick({
        //     dots: false,
        //     infinite: false,
        //     slidesToShow: 5,
        //     slidesToScroll: 1,
        //     arrows: false,
        //     responsive: [
        //         {
        //             breakpoint: 768,
        //             settings: {
        //                 slidesToShow: 2,
        //                 slidesToScroll: 1,
        //                 infinite: false,
        //                 dots: false,
        //                 arrows: false,
        //             },
        //         }
        //     ]
        // });
    </script>

@endsection
