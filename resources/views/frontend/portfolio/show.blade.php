@extends('layouts.frontend-v2')

@section('head_and_title')
<meta name="description" content="BNW Portfolio">
<meta name="author" content="PT. Generasi Muda Gigih">
<meta name="keywords"
    content="Ben and Wyatt, Branding House, Design House, Office, Residential, Apartment, Commercial, Spaces">

<title>BNW - PORTFOLIO</title>
@endsection

@section('content')
<section class=" px-md-0 px-3 pt-5 mt-md-4 spacing-show" style="background-color: black;">
    <div class="container container-fluid text-white">
        @if($type==1)
            <div class="row">
                <div class="col-md-5 col-12">
                    <img src="{{ asset(('images/bnw/portfolio/bnw-web_3.0-13.jpg')) }}" class="img-portfolio pb-3">
                </div>
                <div class="col-md-7 col-12">
                    <div class="row pb-5">
                        <div class="col-md-12">
                            <p class="t1-b-1 custom-font-acaslon-regular mb-4 txt-header-contact">Porto Name
                            </p>
                            <p class="custom-font-avenir-medium text-left">
                                Porto Desc
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Client</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Name</p>
                        </div>
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Brief</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Brief</p>
                        </div>
                    </div>
                </div>
            </div>
        @elseif($type==2)
            <div class="row">
                <div class="col-md-5 col-12">
                    <img src="{{ asset(('images/bnw/portfolio/bnw-web_3.0-14.jpg')) }}" class="img-portfolio pb-3">
                </div>
                <div class="col-md-7 col-12">
                    <div class="row pb-5">
                        <div class="col-md-12">
                            <p class="t1-b-1 custom-font-acaslon-regular mb-4 txt-header-contact">Porto Name
                            </p>
                            <p class="custom-font-avenir-medium text-left">
                                Porto Desc
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Client</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Name</p>
                        </div>
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Brief</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Brief</p>
                        </div>
                    </div>
                </div>
            </div>
        @elseif($type==3)
            <div class="row">
                <div class="col-md-5 col-12">
                    <img src="{{ asset(('images/bnw/portfolio/bnw-web_3.0-15.jpg')) }}" class="img-portfolio pb-3">
                </div>
                <div class="col-md-7 col-12">
                    <div class="row pb-5">
                        <div class="col-md-12">
                            <p class="t1-b-1 custom-font-acaslon-regular mb-4 txt-header-contact">Porto Name
                            </p>
                            <p class="custom-font-avenir-medium text-left">
                                Porto Desc
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Client</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Name</p>
                        </div>
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Brief</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Brief</p>
                        </div>
                    </div>
                </div>
            </div>
        @elseif($type==4)
            <div class="row">
                <div class="col-md-5 col-12">
                    <img src="{{ asset(('images/bnw/portfolio/bnw-web_3.0-16.jpg')) }}" class="img-portfolio pb-3">
                </div>
                <div class="col-md-7 col-12">
                    <div class="row pb-5">
                        <div class="col-md-12">
                            <p class="t1-b-1 custom-font-acaslon-regular mb-4 txt-header-contact">Porto Name
                            </p>
                            <p class="custom-font-avenir-medium text-left">
                                Porto Desc
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Client</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Name</p>
                        </div>
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Brief</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Brief</p>
                        </div>
                    </div>
                </div>
            </div>
        @elseif($type==5)
            <div class="row">
                <div class="col-md-5 col-12">
                    <img src="{{ asset(('images/bnw/portfolio/bnw-web_3.0-17.jpg')) }}" class="img-portfolio pb-3">
                </div>
                <div class="col-md-7 col-12">
                    <div class="row pb-5">
                        <div class="col-md-12">
                            <p class="t1-b-1 custom-font-acaslon-regular mb-4 txt-header-contact">Porto Name
                            </p>
                            <p class="custom-font-avenir-medium text-left">
                                Porto Desc
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Client</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Name</p>
                        </div>
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Brief</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Brief</p>
                        </div>
                    </div>
                </div>
            </div>
        @elseif($type==6)
            <div class="row">
                <div class="col-md-5 col-12">
                    <img src="{{ asset(('images/bnw/portfolio/bnw-web_3.0-18.jpg')) }}" class="img-portfolio pb-3">
                </div>
                <div class="col-md-7 col-12">
                    <div class="row pb-5">
                        <div class="col-md-12">
                            <p class="t1-b-1 custom-font-acaslon-regular mb-4 txt-header-contact">Porto Name
                            </p>
                            <p class="custom-font-avenir-medium text-left">
                                Porto Desc
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Client</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Name</p>
                        </div>
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Brief</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Brief</p>
                        </div>
                    </div>
                </div>
            </div>
        @elseif($type==7)
            <div class="row">
                <div class="col-md-5 col-12">
                    <img src="{{ asset(('images/bnw/portfolio/bnw-web_3.0-19.jpg')) }}" class="img-portfolio pb-3">
                </div>
                <div class="col-md-7 col-12">
                    <div class="row pb-5">
                        <div class="col-md-12">
                            <p class="t1-b-1 custom-font-acaslon-regular mb-4 txt-header-contact">Porto Name
                            </p>
                            <p class="custom-font-avenir-medium text-left">
                                Porto Desc
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Client</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Name</p>
                        </div>
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Brief</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Brief</p>
                        </div>
                    </div>
                </div>
            </div>
        @elseif($type==8)
            <div class="row">
                <div class="col-md-5 col-12">
                    <img src="{{ asset(('images/bnw/portfolio/bnw-web_3.0-20.jpg')) }}" class="img-portfolio pb-3">
                </div>
                <div class="col-md-7 col-12">
                    <div class="row pb-5">
                        <div class="col-md-12">
                            <p class="t1-b-1 custom-font-acaslon-regular mb-4 txt-header-contact">Porto Name
                            </p>
                            <p class="custom-font-avenir-medium text-left">
                                Porto Desc
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Client</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Name</p>
                        </div>
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Brief</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Brief</p>
                        </div>
                    </div>
                </div>
            </div>
        @elseif($type==9)
            <div class="row">
                <div class="col-md-5 col-12">
                    <img src="{{ asset(('images/bnw/portfolio/bnw-web_3.0-27.jpg')) }}" class="img-portfolio pb-3">
                </div>
                <div class="col-md-7 col-12">
                    <div class="row pb-5">
                        <div class="col-md-12">
                            <p class="t1-b-1 custom-font-acaslon-regular mb-4 txt-header-contact">Porto Name
                            </p>
                            <p class="custom-font-avenir-medium text-left">
                                Porto Desc
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Client</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Name</p>
                        </div>
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Brief</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Brief</p>
                        </div>
                    </div>
                </div>
            </div>
        @elseif($type==10)
            <div class="row">
                <div class="col-md-5 col-12">
                    <img src="{{ asset(('images/bnw/portfolio/bnw-web_3.0-28.jpg')) }}" class="img-portfolio pb-3">
                </div>
                <div class="col-md-7 col-12">
                    <div class="row pb-5">
                        <div class="col-md-12">
                            <p class="t1-b-1 custom-font-acaslon-regular mb-4 txt-header-contact">Porto Name
                            </p>
                            <p class="custom-font-avenir-medium text-left">
                                Porto Desc
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Client</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Name</p>
                        </div>
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Brief</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Brief</p>
                        </div>
                    </div>
                </div>
            </div>
        @elseif($type==11)
            <div class="row">
                <div class="col-md-5 col-12">
                    <img src="{{ asset(('images/bnw/portfolio/bnw-web_3.0-29.jpg')) }}" class="img-portfolio pb-3">
                </div>
                <div class="col-md-7 col-12">
                    <div class="row pb-5">
                        <div class="col-md-12">
                            <p class="t1-b-1 custom-font-acaslon-regular mb-4 txt-header-contact">Porto Name
                            </p>
                            <p class="custom-font-avenir-medium text-left">
                                Porto Desc
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Client</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Name</p>
                        </div>
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Brief</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Brief</p>
                        </div>
                    </div>
                </div>
            </div>
        @elseif($type==12)
            <div class="row">
                <div class="col-md-5 col-12">
                    <img src="{{ asset(('images/bnw/portfolio/bnw-web_3.0-30.jpg')) }}" class="img-portfolio pb-3">
                </div>
                <div class="col-md-7 col-12">
                    <div class="row pb-5">
                        <div class="col-md-12">
                            <p class="t1-b-1 custom-font-acaslon-regular mb-4 txt-header-contact">Porto Name
                            </p>
                            <p class="custom-font-avenir-medium text-left">
                                Porto Desc
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Client</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Name</p>
                        </div>
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Brief</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Brief</p>
                        </div>
                    </div>
                </div>
            </div>
        @elseif($type==13)
            <div class="row">
                <div class="col-md-5 col-12">
                    <img src="{{ asset(('images/bnw/portfolio/bnw-web_3.0-31.jpg')) }}" class="img-portfolio pb-3">
                </div>
                <div class="col-md-7 col-12">
                    <div class="row pb-5">
                        <div class="col-md-12">
                            <p class="t1-b-1 custom-font-acaslon-regular mb-4 txt-header-contact">Porto Name
                            </p>
                            <p class="custom-font-avenir-medium text-left">
                                Porto Desc
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Client</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Name</p>
                        </div>
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Brief</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Brief</p>
                        </div>
                    </div>
                </div>
            </div>
        @elseif($type==14)
            <div class="row">
                <div class="col-md-5 col-12">
                    <img src="{{ asset(('images/bnw/portfolio/bnw-web_3.0-32.jpg')) }}" class="img-portfolio pb-3">
                </div>
                <div class="col-md-7 col-12">
                    <div class="row pb-5">
                        <div class="col-md-12">
                            <p class="t1-b-1 custom-font-acaslon-regular mb-4 txt-header-contact">Porto Name
                            </p>
                            <p class="custom-font-avenir-medium text-left">
                                Porto Desc
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Client</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Name</p>
                        </div>
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Brief</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Brief</p>
                        </div>
                    </div>
                </div>
            </div>
        @elseif($type==15)
            <div class="row">
                <div class="col-md-5 col-12">
                    <img src="{{ asset(('images/bnw/portfolio/bnw-web_3.0-33.jpg')) }}" class="img-portfolio pb-3">
                </div>
                <div class="col-md-7 col-12">
                    <div class="row pb-5">
                        <div class="col-md-12">
                            <p class="t1-b-1 custom-font-acaslon-regular mb-4 txt-header-contact">Porto Name
                            </p>
                            <p class="custom-font-avenir-medium text-left">
                                Porto Desc
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Client</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Name</p>
                        </div>
                        <div class="col-md-6 col-6 text-left portfolio-detail">
                            <p class="custom-font-acaslon-regular txt-portfolio-details pt-4">Brief</p>
                            <p class="custom-font-avenir-medium txt-proud">Client Brief</p>
                        </div>
                    </div>
                </div>
            </div>

        @endif
    </div>
</section>
@endsection
@section('styles')
<style>
    .img-portfolio {
        height: 100%;
        /*margin-left: -28px;*/
    }

    .txt-subheader-contact {
        font-size: 18px;
    }

    .btn-paulmarc-more .w-25 {
        width: 75% !important;
    }

    .txt-header-contact {
        font-size: 26px;
        text-align: left;
        margin-bottom: 50px;
    }

    .txt-portfolio-details {
        font-size: 20px;
        font-weight: bold;

    }

    @media (min-width: 576px) {}

    @media (min-width: 768px) {
        .spacing-show{
            margin-top:-100px !important;
            padding-top: 150px !important;

        }

        .portfolio-detail {
            /*padding-left: 70px;*/
        }

        .img-about {
            max-width: 550px;
        }

        .img-bnw {
            width: 100px;
        }

        .img-portfolio {
            height: 100%;
            margin-left: 0px;
        }

        .padding-portfolio {
            padding-top: 4%;
            padding-left: 50px;
        }

        .header-img {
            min-height: 361px !important;
        }

        .btn-paulmarc-more .w-25 {
            width: 25% !important;
        }

        .txt-header-contact {
            font-size: 30px;
            text-align: left;
            margin-bottom: 50px;
        }

        .txt-header-about {
            font-size: 30px;
            margin-bottom: 10px;

        }
    }

    @media (min-width: 992px) {}

    @media (min-width: 1200px) {
        .header-img {
            min-height: 500px;
        }
    }
</style>
@endsection

@section('scripts')
@endsection
