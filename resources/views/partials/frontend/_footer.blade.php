<!-- Footer -->
<footer>
    <div id="contact" class="foot-padds wrapper-desktop" style="background: black;">
        {{--Desktop Footer--}}
        {{--Normal--}}
        <div class="container wrapper-normal d-none d-md-block text-white">
            {{--1st Row--}}
            <div class="row">
                <div class="col">
                    <img src="{{ asset('images/bnw/bnw-web_3.0-04.png') }}" class="w-50">
                    {{--                        <p class="invisible font-didot txt-what" style="position: relative; left: 0;top: 0;">Contact Us</p>--}}
                </div>

                <div class="col">
                    <p class="font-didot txt-section-title">Contact Us</p>
                </div>

                <div class="col"></div>

                <div class="col"></div>

                <div class="col">
                    <p class="font-didot txt-section-title">Quick Reach</p>
                </div>
            </div>
            {{--2nd Row--}}
            <div class="row pt-3 font-av-next">

                <div class="col"></div>

                <div class="col">
                    <span class="txt-section-subtitle">PHONE</span>
                </div>

                <div class="col">
                    <span class="txt-section-subtitle">EMAIL</span>
                </div>

                <div class="col"></div>

                <div class="col">
                    <span class="txt-section-subtitle">CLICK THE BUTTON BELOW</span>
                </div>

            </div>

            <div class="row">
                <div class="col-12" style="height: 30px;">

                </div>
            </div>

            {{--3rd Row--}}
            <div class="row font-av-next txt-section-body">

                <div class="col"></div>

                <div class="col pt-1">
                    <span>+62 822 9982 3888</span>
                </div>

                <div class="col pt-1">
                    <span>info@benandwyatt.com</span>
                </div>

                <div class="col"></div>

                <div class="col pt-3">
                    <a href="http://wa.me/6282299823888">
                        <img src="{{ asset('images/bnw/bnw-web_3.0-24.png') }}" class="wa-btn" alt="">
                    </a>
                </div>
            </div>
            {{--4th Row--}}
            <div class="row pt-3 font-av-next txt-section-foot">

                <div class="col">
                    <p>Ben & Wyatt<br/>
                        Design House</p>
                </div>

                <div class="col">
                    <p>Monday-Friday<br/>
                        9.00am-17.00pm</p>
                </div>

                <div class="col">
                    <p>SOHO Brooklyn Alam Sutera<br/>
                        Tower A , 5th Floor, Unit C</p>
                </div>

                <div class="col">{{--Let Empty--}}</div>

                <div class="col">
                    <p>It will take you directly to<br/>
                        our Whatsapp chat room.</p>
                </div>
            </div>

        </div>

        {{--IG--}}
        <div class="container wrapper-ig d-none d-md-block text-white" style="pointer-events: none">
            {{--1st Row--}}
            <div class="row invisible">
                <div class="col">
                    <img src="{{ asset('images/bnw/bnw-web_3.0-04.png') }}" class="w-50">
                    {{--                        <p class="invisible font-didot txt-what" style="position: relative; left: 0;top: 0;">Contact Us</p>--}}
                </div>

                <div class="col">
                    <p class="font-didot txt-section-title">Contact</p>
                </div>

                <div class="col"></div>

                <div class="col">
                </div>

                <div class="col">
                    <p class="font-didot txt-section-title">Quick Reach</p>
                </div>
            </div>
            {{--2nd Row--}}
            <div class="row pt-3 font-av-next">

                <div class="col"></div>

                <div class="col">
                    <span class="txt-section-subtitle"></span>
                </div>

                <div class="col">
                    <span class="txt-section-subtitle"></span>
                </div>

                <div class="col">
                    <p class="txt-section-subtitle ">INSTAGRAM</p>
                </div>

                <div class="col">
                    <span class="txt-section-subtitle"></span>
                </div>

            </div>
            {{--3rd Row--}}
            <div class="row font-av-next">

                <div class="col"></div>

                <div class="col"></div>

                <div class="col"></div>

                <div class="col" style="pointer-events: auto">
                    {{--<iframe class="pt-1" src="https://snapwidget.com/embed/833711" class="snapwidget-widget"
                            allowtransparency="true" frameborder="0" scrolling="no"
                            style="border:none; overflow:hidden;  width:100%; "></iframe>--}}<!-- SnapWidget -->
                    <iframe src="https://snapwidget.com/embed/836045" class="snapwidget-widget"
                            allowtransparency="true" frameborder="0" scrolling="no"
                            style="border:none; overflow:hidden;  width:100%; "></iframe>
                </div>

                <div class="col pt-3">
                </div>
            </div>
        </div>

        {{--Mobile Footer--}}
        <div class="container d-block d-md-none text-white">
            <div class="row pb-4">
                <div class="col-5">
                    <p class="font-didot txt-what">Contact Us</p>
                </div>
                <div class="col-7">
                    <a href="http://wa.me/6282299823888">
                        <img src="{{ asset('images/bnw/bnw-web_3.0-24.png') }}" class="w-50" alt="">
                    </a>
                </div>
            </div>
            <div class="row pb-4 font-av-next txt-body">
                <div class="col-5">
                    <p>PHONE</p>
                    <p>+62 822 9982 3888</p>
                </div>
                <div class="col-7">
                    <p>EMAIL</p>
                    <p>info@benandwyatt.com</p>
                </div>
            </div>
            <div class="row font-av-next txt-body">
                <div class="col-5">
                    <p>Monday - Friday</p>
                    <p>9:00am - 17:00pm</p>
                </div>
                <div class="col-7">
                    <p>SOHO Brooklyn Alam Sutera</p>
                    <p>Tower A , 5th Floor, Unit C</p>
                </div>
            </div>
        </div>
    </div>
</footer>
@section('styles')
    <style>
        .wrapper-desktop {
            position: relative;
        }

        .wrapper-normal {

        }

        .wrapper-ig {
            padding-top: 100px;
            z-index: 9;
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
        }

        .wa-btn {
            max-width: 70%;
        }

        .txt-section-title {
            font-size: 20px;
        }

        .txt-section-subtitle {
            font-size: 13px;
        }

        .txt-section-body {
            font-size: 13px;
        }

        .txt-section-foot {
            font-size: 12px;
            line-height: 1.8em;
        }

        @media (max-width: 991px) {

            .txt-section-title {
                font-size: 18px;
            }

            .txt-section-subtitle {
                font-size: 11px;
            }

            .txt-section-body {
                font-size: 10px;
            }

            .txt-section-foot {
                font-size: 8px;
            }

        }
    </style>

    <!-- SnapWidget -->
    <script src="https://snapwidget.com/js/snapwidget.js"></script>
