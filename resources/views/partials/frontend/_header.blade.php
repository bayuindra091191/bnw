<!-- Header -->
<header>
    {{--Desktop--}}
    <div class="d-none d-md-block">
        <!-- Header desktop STILL -->
    {{-- <nav class="container-header-desktop" id="header-still">
        <!-- Logo desktop -->
        <div class="wrap-menu-desktop">
            <div class="limiter-menu-desktop container">
                <a href="{{ route('home') }}" style="margin: 0 auto 0; margin-top: -100px">
    <img src="{{ asset('images/marc/about/Marcs Website components-14.png') }}" alt="LOGO" height="50">
    </a>
    <!-- Menu desktop -->
    <div class="menu-desktop w-100" style="position: absolute; left: 0;">
        <ul class="main-menu respon-sub-menu" style="margin: 0 auto 0;">
            <li>
                <a class="custom-font-1 menu-font-style" href="{{ route('home') }}"
                    @if(Route::currentRouteName()=='home' )
                    style="border-bottom: 1px solid #000; padding-bottom: 10px;" @endif>HOME</a>
            </li>
            <li>
                <a class="custom-font-1 menu-font-style" href="{{ route('frontend.about') }}"
                    @if(Route::currentRouteName()=='frontend.about' )
                    style="border-bottom: 1px solid #000; padding-bottom: 10px;" @endif>ABOUT</a>

            </li>

            <li>
                @if(Route::currentRouteName() == 'frontend.district' ||
                Route::currentRouteName() == 'frontend.paul.introduction' ||
                Route::currentRouteName() == 'frontend.paul.prives' ||
                Route::currentRouteName() == 'frontend.paul.lanes')
                <a class="custom-font-1 menu-font-style" href="{{ route('frontend.district') }}"
                    style="border-bottom: 1px solid #000; padding-bottom: 10px;">DISTRICT <i
                        class="fa fa-chevron-down"></i></a>
                @else
                <a class="custom-font-1 menu-font-style" href="{{ route('frontend.district') }}">DISTRICT <i
                        class="fa fa-chevron-down"></i></a>
                @endif
                <ul class="sub-menu" style="z-index:9999;">
                    <li><a href="{{ route('frontend.paul.introduction') }}" class="custom-font-1">PAUL
                            MARC</a></li>
                    <li><a href="#" class="custom-font-1 disabled-link">DEAN MARC</a></li>
                    <li><a href="#" class="custom-font-1 disabled-link">GRANT MARC</a></li>
                    <li><a href="#" class="custom-font-1 disabled-link">WILL MARC</a></li>
                    <li><a href="#" class="custom-font-1 disabled-link">OLENN MARC</a></li>
                </ul>
            </li>

            <li>
                <a class="custom-font-1 menu-font-style" href="{{ route('frontend.developer') }}"
                    @if(Route::currentRouteName()=='frontend.developer' )
                    style="border-bottom: 1px solid #000; padding-bottom: 10px;" @endif>DEVELOPER</a>
            </li>

            <li>
                <a class="custom-font-1 menu-font-style" href="{{ route('frontend.masterplan') }}"
                    @if(Route::currentRouteName()=='frontend.masterplan' )
                    style="border-bottom: 1px solid #000; padding-bottom: 10px;" @endif>MASTERPLAN</a>
            </li>

            <li>
                <a class="custom-font-1 menu-font-style" href="{{ route('frontend.partners') }}"
                    @if(Route::currentRouteName()=='frontend.partners' )
                    style="border-bottom: 1px solid #000; padding-bottom: 10px;" @endif>PARTNERS</a>
            </li>

            <li>
                <a class="custom-font-1 menu-font-style" href="{{ route('frontend.contact_us') }}"
                    style="padding-right: 20px !important;" @if(Route::currentRouteName()=='frontend.contact_us' )
                    style="border-bottom: 1px solid #000; padding-bottom: 10px;" @endif>CONTACT</a>
            </li>
        </ul>
    </div>
</div>
</div>
</nav> --}}

    <!-- Header desktop STICKY -->
        <nav class="container-header-desktop" id="header-sticky">
            <div class="wrap-menu-desktop" style="background-color: rgba(0,0,0,0.7) !important;">
                <div class="container">
                    <div class="row " style=" height: 100px;">
                        <!-- Logo desktop -->
                        <div class="logo-header col-3 d-flex">
                            <a href="{{ route('home') }}" class="align-self-center">
                                <img src="{{ asset('images/bnw/bnw-web_3.0-04.png') }}" alt="LOGO"
                                     style="width: 70px; height: auto">
                            </a>
                        </div>
                        <!-- Menu desktop -->
                        <div class="menu-desktop col-6">
                            <div>
                                <ul class="main-menu">
                                    <li>
                                        <a class="font-av-next menu-font-style text-white"
                                           href="{{ route('home') }}">
                                            <div class="text-white">
                                                <span class=" font-header-menu">Home</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a class="font-av-next menu-font-style text-white"
                                           href="{{ route('frontend.portfolio') }}">
                                            <div class="text-white">
                                                <span class=" font-header-menu">Works</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a onclick="navigateSection('services')"
                                           class="font-av-next menu-font-style text-white"
                                           href="#">
                                            <div class="text-white">
                                                <span class=" font-header-menu">Services</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a onclick="navigateSection('clients')"
                                           class="font-av-next menu-font-style text-white"
                                           href="#">
                                            <div class="text-white">
                                                <span class=" font-header-menu">Clients</span>
                                            </div>
                                        </a>
                                    </li>
                                    <li>
                                        <a onclick="navigateSection('contact')"
                                           class="font-av-next menu-font-style text-white"
                                           href="#">
                                            <div class="text-white">
                                                <span class="font-header-menu">Contact</span>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="col-3 d-flex justify-content-end">
                            <div class="font-av-next font-header-menu text-white align-self-center float-right">
                                <a class="text-white" href="http://wa.me/6282299823888">
                                    Click Here for Quick Reach
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>

    <!-- Mobile -->
    <nav class="d-block d-md-none">
        <div class="wrap-header-mobile" style="background-color: rgba(0,0,0,0) !important; padding: 0px !important;">
            <!-- Logo mobile -->
            <div class="menu-mobile" style="width: 100%; z-index: 9999999;">
                {{--Logo--}}
                <div class="logo-header" style="margin-left: 2.2em;">
                    <a style="display:inline-flex;width:85px;" href="{{ route('home') }}"><img
                            src="{{ asset('images/bnw/bnw-web_3.0-04.png') }}" style="width: auto; height: 28px"
                            alt="LOGO"></a>
                </div>
                {{--Menus--}}
                <div class="menu-mobile">
                    <div>
                        <ul class="main-menu">
                            <li><a class="font-av-next text-white"
                                   href="{{ route('home') }}">Home</a></li>
                            <li><a class="font-av-next text-white"
                                   href="{{ route('frontend.portfolio') }}">Works</a></li>
                        </ul>
                    </div>
                </div>
                {{--WA--}}
                <div class="wa-wrapper mr-3"><a href="http://wa.me/6282299823888">
                        <img src="{{ asset('images/bnw/bnw-web_3.0-24.png') }}" class="w-100 mt-2 mb-2 wa-img"
                            {{--style="max-width: 100%; height: auto"--}}>
                    </a>
                </div>
            </div>


            <!-- Button show menu -->
            {{--            <div class="btn-show-menu-mobile hamburger hamburger--squeeze">--}}
            {{--                <span class="hamburger-box">--}}
            {{--                    <span class="hamburger-inner"></span>--}}
            {{--                </span>--}}
            {{--            </div>--}}
        </div>


    </nav>
</header>

@section('styles')
    <style>
        .wa-wrapper {
            height: 100%;
        }

        .logo-header {
            height: 65%;
            width: auto !important;
            flex-wrap: wrap;
        }

        /*.quick-reach:link {*/
        /*    color: red;*/
        /*}*/

        /*.quick-reach:visited {*/
        /*    color: green;*/
        /*}*/

        /*.quick-reach{*/
        /*    color: white !important;*/
        /*}*/

        a:hover {
            text-decoration: underline;
            color: white;
        }

        a:active {
            text-decoration: underline;
            color: white;
        }

        .menu-desktop-header {
            display: -webkit-box;
            display: -webkit-flex;
            display: -moz-box;
            display: -ms-flexbox;
            display: flex;
            align-items: center;
            height: 100%;
        }
    </style>
@endsection

@section('scripts')
    <script>
        $(function () {
            $('.wa-img').load(function () {
                var $img = $(this);
                $img.attr('max-width', $img.width()).attr('max-height', $img.height());
            });
        });
    </script>
@endsection
